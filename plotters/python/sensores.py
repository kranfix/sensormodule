#!/usr/bin/python3
import serial
import numpy as np
from matplotlib import pyplot as plt
import time
from math import sin, pi

#s = serial.Serial('/dev/ttyUSB0', 9600)
s = serial.Serial('/dev/ttyACM0', 9600)

def leer(s,B):
	s.write(B)
	a = s.readline().rstrip()
	while a[0] in b'yn':
        	a = a[1:]
	return a

ax1 = plt.axes()
plt.ion()

#monoxido
n1 = 50
y1 = [30] * n1
plt.subplot (221)	
plt.xlabel('Tiempo (s)--->')
plt.ylabel('Monoxido (ppm)--->')
line1, = plt.plot(y1, color = 'green')
plt.ylim([20,60])
plt.xlim([-n1,0])
#temperatura
n2 = 10
y2 = [25] * n2
plt.subplot(222)
plt.xlabel('Tiempo (s)--->')
plt.ylabel('Temperatura (°C) --->')
line2, = plt.plot(y2, color = 'red')	
plt.ylim([25,30])
plt.xlim([-n2,0])
#humedad
n3 = 70
y3 = [90] * n3
plt.subplot(223)
plt.xlabel('Tiempo (s)--->')
plt.ylabel('Humedad (%) --->')
line3, = plt.plot(y3, color = 'yellow')
plt.ylim([20,60])
plt.xlim([-n3,0])
#presion
n4 = 20
y4 = [99] * n4
plt.subplot(224)
plt.xlabel('Tiempo (s)--->')
plt.ylabel('Presion (kPa) --->')
line4, = plt.plot(y4, color= 'blue')
plt.ylim([80,120])
plt.xlim([-n4,0])


i = 0;
while True:
	#MONOXIDO
	plt.subplot(221)
	del y1[0]
	y1.append(int(leer(s,b'c')))
	plt.ylim([float(min(y1))-10, float(max(y1))+10])
	plt.xlim([i-n1+1, i])
	line1.set_xdata(np.arange(i-n1+1,i+1))
	line1.set_ydata(y1)
	#TEMPERATURA
	plt.subplot(222)
	plt.ylim([float(min(y2))-10, float(max(y2))+10])
	del y2[0]
	y2.append(float(leer(s,b't')))
	plt.xlim([i-n2+1, i])
	line2.set_xdata(np.arange(i-n2+1,i+1))
	line2.set_ydata(y2)	
	#HUMEDAD
	plt.subplot(223)
	del y3[0]
	y3.append(float(leer(s,b'h')))
	plt.ylim([float(min(y3))-2, float(max(y3))+2])
	plt.xlim([i-n3+1, i])
	line3.set_xdata(np.arange(i-n3+1,i+1))
	line3.set_ydata(y3)
	#PRESION
	plt.subplot(224)
	del y4[0]
	y4.append(float(leer(s,b'p')))
	plt.ylim([float(min(y4))-1, float(max(y4))+1])
	plt.xlim([i-n4+1, i])
	line4.set_xdata(np.arange(i-n4+1,i+1))
	line4.set_ydata(y4)
	# Draw	
	plt.draw()
	i = i + 1	
