//moduo de sensores
//libreria para el sensor de humedad
#include "DHT.h"
#define DHTPIN 6
#define DHTTYPE DHT11
DHT dht(DHTPIN,DHTTYPE);

//librerias para el sensor de presion
#include "MPL115A1.h"
#include <SPI.h>

MPL115A1_t mpl;

float h = 0;
float r = 0;

float temp_c = 0;
float pressure = 0;

char var = 'S';
boolean a = false;
int co2 = 0;

void setup() {
  Serial.begin(9600);
  dht.begin();
  mpl.begin();
}

void loop() 
{
  boolean a = Serial.available();
 // Serial.println(a);
 
  while(Serial.available()){ 
   // Serial.println(a);
    if(!a) {
      a = true;
      Serial.write('n');
   //   var = Serial.read();
    } 
      Serial.write('y');
    
   // Serial.write('y');
      var = (char)Serial.read();
      switch(var)
      {
      case 'c'://Registra el CO2
        //MQ7
          co2 = analogRead(A0);//0 a 1024
          Serial.println(co2); //MQ7
          break;
      
      case 'h'://Registra la humedad
        h = dht.readHumidity();
        Serial.println(h);
      break;
      
      /*case 'r'://Registra la humedad relativa
        h.i = dht.readHumidity();
       // delay(250);
        float t = dht.readTemperature();
       // Read temperature as Fahrenheit
        float f = dht.readTemperature(true);
        r.j = dht.computeHeatIndex(f, h.i);
        Serial.println(r.j);
      break;*/
       
      case 't'://Registra la temperatura
        temp_c = mpl.Celcius();
        Serial.println(temp_c);
      break;
      
      case 'p':
        pressure = mpl.Pressure();
        Serial.println(pressure);
      break;
      
      case 'r'://Registra la humedad relativa
        h = dht.readHumidity();
       // delay(250);
        float te = dht.readTemperature();
       // Read temperature as Fahrenheit
        float f = dht.readTemperature(true);
        r = dht.computeHeatIndex(f, h);
        Serial.println(r);
      break;
       
      }
      //Serial.print(" \n var = ")
      //Serial.println(var);
      //Serial.print(var);
      Serial.read(); // lee el salto de linea '\n'
      //Serial.println("Recibido");
     // Serial.write('X');
   }
    a = false;
  //else
  //{
   // Serial.print("Enviame otra vez");
  //}
}

