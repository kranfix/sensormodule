#include <MPL115A1.h>
#include <SPI.h>

MPL115A1_t mpl;

void setup() {
  Serial.begin(9600);  
  mpl.begin();
}

void loop() 
{
   Serial.println(mpl.Celcius());
   Serial.println(mpl.Pressure());
   Serial.println(mpl.Farenheit());
   delay(1000);
}
