#include <SPI.h> //libreria SPI incluida en el arduino

#define PRESH   0x80
#define PRESL   0x82
#define TEMPH   0x84
#define TEMPL   0x86

#define A0MSB   0x88
#define A0LSB   0x8A
#define B1MSB   0x8C
#define B1LSB   0x8E
#define B2MSB   0x90
#define B2LSB   0x92
#define C12MSB  0x94
#define C12LSB  0x96

#define CONVERT   0x24   

#define chipSelectPin 8
#define shutDown 7

class MPL115A1_t {
  public:
    float presion;
    float temperatura;
    void begin();
    float Pressure(); // kPa (kilo Pascal)
    float Farenheit();
    float Celcius();
  private:
    float A0_, B1_, B2_, C12_;
    //leer registros
    unsigned int readRegister(byte thisRegister);
};
