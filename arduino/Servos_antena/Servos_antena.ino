/*
HMC5883L_Example.pde - Example sketch for integration with an HMC5883L triple axis magnetomerwe.
Copyright (C) 2011 Love Electronics (loveelectronics.co.uk)

This program is free software: you can redistribute it and/or modify
it under the terms of the version 3 GNU General Public License as
published by the Free Software Foundation.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

// Reference the I2C Library
#include <Wire.h>
#include <Servo.h>
// Reference the HMC5883L Compass Library
#include <HMC5883L.h>

// Store our compass as a variable.
HMC5883L compass;
// Record any errors that may occur in the compass.
int error = 0;


//////probando valores inciales del GPS///////

float x0 = 5;
float y0 = 8;
float z0 = 10;
float x1 = 8;
float y1 = 12;
float z1 = 20;
//////////////////////////////////////////////

Servo servo_phi;//Servo plano XY
Servo servo_theta;//Servo del angulo de elevacion

// Out setup routine, here we will configure the microcontroller and compass.
void setup()
{
  // Initialize the serial port.
  Serial.begin(9600);
  
  //Assign pines
  servo_phi.attach(9);
  servo_theta.attach(7);
  
  Serial.println("Starting the I2C interface.");
  Wire.begin(); // Start the I2C interface.

  Serial.println("Constructing new HMC5883L");
  compass = HMC5883L(); // Construct a new HMC5883 compass.
    
  Serial.println("Setting scale to +/- 1.3 Ga");
  error = compass.SetScale(1.3); // Set the scale of the compass.
  if(error != 0) // If there is an error, print it out.
    Serial.println(compass.GetErrorText(error));
  
  Serial.println("Setting measurement mode to continous.");
  error = compass.SetMeasurementMode(Measurement_Continuous); // Set the measurement mode to Continuous
  if(error != 0) // If there is an error, print it out.
    Serial.println(compass.GetErrorText(error));
    
  //First lecture
  float init_angle = 0;
  init_angle = ReadMagn();
    
  //Setting servo_phi to the Nort
  servo_phi.write(int(init_angle));
  
}

// Our main program loop.
void loop()
{
  //FUNCION DEL GPS DEL DRONE QUE ME ARROJE LOS VALORES DE X0 ,Y0 ,Z0 ,X1 ,Y1 ,Z1
  
  double theta = Ang_theta(x0,y0,z0,x1,y1,z1);
  double phi = Ang_phi(x0,y0,x1,y1);
  
  servo_phi.write(phi);
  servo_theta.write(theta);
  
  // Output the data via the serial port.
  ////Output(raw, scaled, heading, headingDegrees);
  
  // Normally we would delay the application by 66ms to allow the loop
  // to run at 15Hz (default bandwidth for the HMC5883L).
  // However since we have a long serial out (104ms at 9600) we will let
  // it run at its natural speed.
  // delay(66);
}



// Output the data down the serial port.
void Output(MagnetometerRaw raw, MagnetometerScaled scaled, float heading, float headingDegrees)
{
   Serial.print("Raw:\t");
   Serial.print(raw.XAxis);
   Serial.print("   ");   
   Serial.print(raw.YAxis);
   Serial.print("   ");   
   Serial.print(raw.ZAxis);
   Serial.print("   \tScaled:\t");
   
   Serial.print(scaled.XAxis);
   Serial.print("   ");   
   Serial.print(scaled.YAxis);
   Serial.print("   ");   
   Serial.print(scaled.ZAxis);

   Serial.print("   \tHeading:\t");
   Serial.print(heading);
   Serial.print(" Radians   \t");
   Serial.print(headingDegrees);
   Serial.println(" Degrees   \t");
}

float ReadMagn()
{
  // Retrive the raw values from the compass (not scaled).
  MagnetometerRaw raw = compass.ReadRawAxis();
  // Retrived the scaled values from the compass (scaled to the configured scale).
  MagnetometerScaled scaled = compass.ReadScaledAxis();
  
  // Values are accessed like so:
  int MilliGauss_OnThe_XAxis = scaled.XAxis;// (or YAxis, or ZAxis)

  // Calculate heading when the magnetometer is level, then correct for signs of axis.
  float heading = atan2(scaled.YAxis, scaled.XAxis);
  
  // Once you have your heading, you must then add your 'Declination Angle', which is the 'Error' of the magnetic field in your location.
  // Find yours here: http://www.magnetic-declination.com/
  // Mine is: 2� 37' W, which is 2.617 Degrees, or (which we need) 0.0456752665 radians, I will use 0.0457
  // If you cannot find your Declination, comment out these two lines, your compass will be slightly off.
  float declinationAngle = 0.0457;
  heading += declinationAngle;
  
  // Correct for when signs are reversed.
  if(heading < 0)
    heading += 2*PI;
    
  // Check for wrap due to addition of declination.
  if(heading > 2*PI)
    heading -= 2*PI;
   
  // Convert radians to degrees for readability.
  float headingDegrees = heading * 180/M_PI; 
  
  return headingDegrees;
}
/*
double F_Angle(float x0,float y0,float z0,float x1,float y1,float z1)
{
  float theta = 0;
  float phi = 0; 
  
  float xr = x1-x0;
  float yr = y1-y0;
  float zr = z1-z0;
  
  double d_xy = sqrt(pow(xr,2)+pow(yr,2));
  
  phi = atan2(yr,xr);
  theta = atan2(zr,d_xy);
  
  float ang[2] = {phi,theta};
  float *m=ang[0];
  
  return m;
}
*/
double Ang_theta(float x0,float y0,float z0,float x1,float y1,float z1)
{
  float theta = 0;
  
  float xr = x1-x0;
  float yr = y1-y0;
  float zr = z1-z0;
  
  double d_xy = sqrt(pow(xr,2)+pow(yr,2));
  
  theta = atan2(zr,d_xy);
  
  return theta;
}

double Ang_phi(float x0,float y0,float x1,float y1)
{
  float phi = 0;
  
  float xr = x1-x0;
  float yr = y1-y0;
  
  phi = atan2(yr,xr);
  
  return phi;
}
